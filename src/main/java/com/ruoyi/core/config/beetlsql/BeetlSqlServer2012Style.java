package com.ruoyi.core.config.beetlsql;

import java.util.Map;

import org.beetl.sql.core.db.SqlServer2012Style;

/**
 * SQL Server 2012以上版本请使用此DBStyle，对翻页做了优化
 * @author darren
 *
 */
public class BeetlSqlServer2012Style extends SqlServer2012Style {

    public BeetlSqlServer2012Style() {
        super();
    }
    @Override
    protected String getOrderBy() {
        //重写getOrderBy，如果设置了分页的order by条件 则按 order by 否则添加一个 current_timestamp 来排序
        return lineSeparator + HOLDER_START + "text(' order by ' + ((has(_orderBy)&&!isEmptyObj(_orderBy))?_orderBy:'current_timestamp'))" + HOLDER_END + " ";
    }

}