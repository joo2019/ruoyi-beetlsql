package com.ruoyi.core.database;

import org.beetl.sql.core.IDAutoGen;

import cn.hutool.core.util.IdUtil;
/**
 * beetl uuid生成
 * @author admin
 *
 */
public class UuidAutoGen implements IDAutoGen<String> {
	public String nextID(String params) {
		return IdUtil.randomUUID();
	}

}
