package com.ruoyi.core.web.intercepter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Splitter;

/**
 * 拦截ruoyi默认列表list查询
 * @author admin
 *
 */
@Component
public class SystemListInterceptor implements HandlerInterceptor {
    private final static Logger logger = LoggerFactory.getLogger(SystemListInterceptor.class);
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    	String url = request.getRequestURI();
    	
    	String newUrl = null;
    	if("/system/role/authUser/allocatedList".equals(url)) {
    		newUrl = "/system/list/role/authuser";
    	}else if("/system/role/authUser/unallocatedList".equals(url)) {
    		newUrl = "/system/list/role/unallocated";
    	}else {
    		List<String> urls = Splitter.on('/').omitEmptyStrings().splitToList(url);
        	
        	if(urls.size() == 4) {
        		newUrl = "/"+urls.get(0)+"/"+urls.get(3)+"/"+urls.get(1)+"/"+urls.get(2);
        	}else {
        		newUrl = "/"+urls.get(0)+"/"+urls.get(2)+"/"+urls.get(1);
        	}
    	}
    	request.getRequestDispatcher(newUrl).forward(request, response);
        return false;
    }
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
    }
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        
    }
}