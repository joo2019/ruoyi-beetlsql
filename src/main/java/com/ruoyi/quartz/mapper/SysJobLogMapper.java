package com.ruoyi.quartz.mapper;

import java.util.Date;
import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.ruoyi.core.database.SqlParam;
import com.ruoyi.quartz.domain.SysJobLog;


/**
 * 调度任务日志信息 数据层
 * 
 * @author ruoyi
 */
@SqlResource("system.sysjoblog")
public interface SysJobLogMapper extends BaseMapper<SysJobLog>
{
    /**
     * 获取quartz调度器日志的计划任务
     * 
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
	public PageQuery<SysJobLog> queryByCondition(PageQuery<SysJobLog> pageQuery);
	
    public default List<SysJobLog> selectJobLogList(SysJobLog jobLog){
    	PageQuery<SysJobLog> page = new PageQuery<>();
    	page.setPageSize(Integer.MAX_VALUE);
        page.setPageNumber(1);
        page.setTotalRow(Integer.MAX_VALUE);
        page.setParas(jobLog);
        return queryByCondition(page).getList();
    }

    /**
     * 查询所有调度任务日志
     *
     * @return 调度任务日志列表
     */
    public default List<SysJobLog> selectJobLogAll(){
    	return this.all();
    }

    /**
     * 通过调度任务日志ID查询调度信息
     * 
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    public default SysJobLog selectJobLogById(Long jobLogId) {
    	return this.single(jobLogId);
    }

    /**
     * 新增任务日志
     * 
     * @param jobLog 调度日志信息
     * @return 结果
     */
    public default int insertJobLog(SysJobLog jobLog) {
    	jobLog.setCreateTime(new Date());
    	this.insert(jobLog,true);
    	return 1;
    }

    /**
     * 批量删除调度日志信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public default int deleteJobLogByIds(String[] ids) {
    	return this.getSQLManager().executeUpdate("delete from sys_job_log where job_log_id  in (#join(ids)#)", SqlParam.create().set("ids", ids));
    }

    /**
     * 删除任务日志
     * 
     * @param jobId 调度日志ID
     * @return 结果
     */
    public default int deleteJobLogById(Long jobId) {
    	return this.deleteById(jobId);
    }

    /**
     * 清空任务日志
     */
    public default void cleanJobLog() {
    	this.executeUpdate("truncate table sys_job_log");
    }

	
}
