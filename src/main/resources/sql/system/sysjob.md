queryByCondition
===
	select 
	@pageTag(){
	*
	@}
	from sys_job where 1=1
	@if(!isBlank(jobName)){
	    and  job_name like #'%'+jobName+'%'#
	@}
	@if(!isBlank(jobGroup)){
	    and  job_group = #jobGroup#
	@}
	@if(!isBlank(status)){
	    and  status = #status#
	@}
	@if(!isBlank(dictType)){
	    and  dict_type like #'%'+dictType+'%'#
	@}
	@if(!isBlank(invokeTarget)){
	    and  invoke_target like #'%'+invokeTarget+'%'#
	@}
	
	@pageIgnoreTag(){
	order by job_id desc
	@}
	
	